---
title: 那年，我只身一人来到上海（一）
date: 2017-11-30 23:23:00
tags:
---
那年，只身一人来到上海。

住着隔板房，就是用木板把卧室，客厅，甚至厨房隔成两间。原本三室的屋子竟被隔出了 6，7间，住着近10个人。炎热的夏天，没有空调，共用着卫生间，共用着阳台。只有一张矮矮的木板床，每月还要交着1500的房租。

后来有一天凌晨，还没有睡醒，就来了拆迁队。说是我们违法群租，遭到热心的居民举报，已经过了最后期限，立即拆除。打电话给房东，原来拆迁队就是房东叫来的，房东表示也没办法，上面要检查，他的多处房产面临要拆除。两个小时后就有领导来突击检查。等检查完了再把还原。后来，有一个在这么住了有段时间的大哥告诉我，这不是第一次了，我没有来之前，就拆过一次。大概两个月来一次把。他很无奈。

我站在那，却一点办法都没有，深深的感受到了自己的无能为力，仿佛地上的蝼蚁，每日忙忙碌碌，也不知到在忙什么，哪一天就面临上帝之脚。

后来领导来了，和他们谈笑风生，就像多年不见的老友一样。领导看着还有一块板在那，说拿锤子来。然后一脸大笑的挥了下去。领导说，拆干净点，下次别让我抓到。有个工人头迎合道：您说的都对，拆和盖都是我们，巴不得领导多来几次呢。原话记不得了，意思是这意思。

晚上和父母通视频，我说没关系，还能住，不碍事。笑着笑着就流下了眼泪。越是想强忍着却越是忍不住。父母急死了，要来上海要看我，我说没事，我还要上班。真的挺好的，就是有点想家，真的只是有点想家而已...

这篇文字我一开始写在 Soul 上，是因为有个话题叫做 一张照片，一个故事。所以我就写了一下。结果收到很多热心网友的评论，看着大家相似的经历，真的很暖心。在这里截一些图贴一下，谢谢各位。

![](https://bucket.jadendong.com/img/I-was-alone-in-Shanghai-that-year01.jpg/default)
![](https://bucket.jadendong.com/img/I-was-alone-in-Shanghai-that-year02.jpg/default)
![](https://bucket.jadendong.com/img/I-was-alone-in-Shanghai-that-year03.jpg/default)
![](https://bucket.jadendong.com/img/I-was-alone-in-Shanghai-that-year04.jpg/default)
![](https://bucket.jadendong.com/img/I-was-alone-in-Shanghai-that-year05.jpg/default)
![](https://bucket.jadendong.com/img/I-was-alone-in-Shanghai-that-year06.jpg/default)
![](https://bucket.jadendong.com/img/I-was-alone-in-Shanghai-that-year07.jpg/default)
![](https://bucket.jadendong.com/img/I-was-alone-in-Shanghai-that-year08.jpg/default)
![](https://bucket.jadendong.com/img/I-was-alone-in-Shanghai-that-year09.jpg/default)
![](https://bucket.jadendong.com/img/I-was-alone-in-Shanghai-that-year10.jpg/default)
![](https://bucket.jadendong.com/img/I-was-alone-in-Shanghai-that-year11.jpg/default)
