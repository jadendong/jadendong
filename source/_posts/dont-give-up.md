---
title: 不要放弃自己！
date: 2018-05-29 17:15:50
tags:
---
现在是 2018 年的 5 月 29 号，凌晨 1 点，我依然没有找到工作。

## 我知道，我现在还不能丧失理智。
## 内向，自卑
2017 年 11 月 我辞去了工作。原因很简单，我适应不了那个环境。我想有一大部分自己的原因把。我的性格，还是太内向。一般导致我内向，害羞的原因是，我觉得我自己实力不行，就是心虚。每当我心虚的时候，我就会逃避一些现实，消磨自己的时间，假装自己很忙的样子。和人说话时，声音很小。加上我从小的口音问题，那就会表述不清，说话没有底气，也许在别人看起来，我很猥琐把。知乎上说自卑的人看起来很猥琐，不是真的指内心猥琐，而是真的猥琐。这一点我完完全全可以证明。而且自卑的人通常走路腰都是驼背。这一点我又一次证明了。其实这些年来我已经改变了很多，我早已不自卑，不内向。内心也慢慢强大起来。第一份工作的时候，刚进去之前我是一点不内向，也没有畏缩的心理。由于这个公司的特殊原因，只有我一个后端，而我什么也不会，也没有人带我。每天办公室里都还挺安静的，我一个人也不知道该怎么办，写代码也写不出来，还要硬着写。啥文档都没留下，关键他们自己都不知道这个改版的商城要有什么效果，有哪些功能都没想清楚，逻辑上的不合理。对了，我每周还要和设计师汇报，没想到把，我一个程序员竟然要向设计师汇报，他什么也不懂，还要每周卡进度，交报告。就我一个人，我也是刚毕业，一个完整的项目都没做过。还有一个前端，她是真前端，后端的东西完全帮不上忙。好了，吐槽到此为止。说我自己的原因，我还是适应不了那个公司，由于我根本完成不了任务，导致我很心虚，在办公室里经常一天都讲不了几句话。出去等开水都是小心翼翼。走路的声音都是轻轻的，生怕别人知道我的存在。所有离开是正确的，不耽误公司，也不耽误自己。只是当时那个时间点离开，是不是正确我就不知道了。

## 我着六个月都干了些啥啊
## 拖延症
第一天睡了一觉，第二天的时候我下载了一款社交软件，并在其中疯狂撩妹，在第八天的时候遇到了一位姑娘，并后来的时间里喜欢上了她。说来可笑，遇到她的时候，我们天天在一起聊天，聊到深夜。还第一次和网友开了视频，没有我想象中的尴尬，反而是抢着说话说，她还在千里之外给我点了个外卖。这件事情点到为止，不细讲。在接下来的两个月的，我的时间都花在这个地方了。我不是怪这个姑娘，如果没有她我也会把时间用在别的地方。当然，我和她最后就凉凉了。辞职之后，我就没有想过要好好复习，我想过，可我一点计划都没有，一点行动力也没有，稍微有别的新鲜事物就会被吸引过去。就这样，转眼到了 2018 年的 1 月份。由于我太过于在乎妹子，每天患得患失，最终成功作死。我甚至还深夜哭过还几次。一个人茫然无措，天天麻痹自己。终于我想到了找工作。唉，我落下太多，在前东家的时候几乎就是每天混日子，所以面试了几家都没有通过。我甚至还报了两家猎头公司，会员费一个 300，一个 500。他们给我推荐的公司还真的不错，可惜我的基础太差，一个也没成功。

现在是凌晨 2.39，我得早点睡觉。有空接着这个写。

## 2018/05/31 杭州 暴雨
今天还算开心，因为下午的面试成功啦！经理叫我下星期一上班。yeah,不过还是有些担心，因为经理走的时候，明确的说了待会发offer。可是一个下午过去了，并没有收到。不会又凉凉吧！谁知道呢，应该不会吧，他没必要忽悠我，我明天中午打电话问问吧。祝我成功，也做好他们临时变卦的可能性吧。

唉，这就要说道我的上周。上周四，也就是 5 月 24 号。我在张江那边面了一家公司。先是技术负责人，然后是 HR 。技术经理对我有点不满意，因为我的基础有点差。但是还是把 HR 叫出来谈薪资。我觉得如果不满意完全没有必要到这一步。然后 HR 也问了我好多，包括家庭情况，以及一个社会热点的看法。然后我临走时还亲切的招呼我快去吃饭吧。整个面试过程持续了一个小时。HR 说询问一下具体工资，8k 一般来说是可以给到的。然后就没有后文了。其实我伤心的是我还挺满意这家公司的，原本是做通信的要进行转型。他们正在搭框架，用 spring cloud,以及一些最新的技术，像 Hadoop ,spark 都有人在搞，他们说的是每天可以预留 20% 的时间用来学习。我想这正好，我可以从头参与进来，可惜了，可惜了，过去了就过去吧。

还有今天早上那家是真的窝心，是个金融公司，严格来说是做贷款的。只有两个技术人，面我的时候像是在逼问我，如果想考察我的能力，直接问我一些题目呀。好吧，我承认我是初级开发者。他们要招的是技术深一点的。但是企图用更少的钱找到更好的工程师，怎么可能。浪费我们大家时间。而且这二位从面容上来看也不是专心做技术的。吐槽到此为止。

## 2018/06/03 凌晨 1 点 累
在周五下午的时候，我收到了offer。超开心的，然后又要找房子了，还有搬家，退租。想想就烦，唉。然后昨天就去咯。我看中了一个离公司不远的房子，不到两公里吧。离地铁站也不到两公里。而且才1290。是这一片最便宜的了。我去的时候，果然很抢手。然而我当即力断，抢下了这房子。啊啊啊啊啊，接下来就很烦了，蛋壳这边的有合同在，杭州的不了，换租的话又是跨区域，也搞不了。前前后后打了一个小时电话，找各种主管。售后，售后又去问主管去。。。然后我放弃了，那就退租吧，然后再签。妈的，退租最快3天。我又急着入住，还有怕房间被抢了。只能找朋友，以他的名义预定三天。一天就这样过去了，周六人真是多啊。